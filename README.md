# Bauen und Anlieferung der Applikation aus einzelnen Teilservices

Ziel dieses Prototypen ist es eine flexible Bereitstellung der Anwendung über verschiendene Wege zu ermöglichen:

- Als Anwendung auf Nicht-Docker-Umgebungen (Alle Umgebungen Stand heute)
- Über docker-compose fur lokale Entwicklung/lokale Umgebungen bzw. Kunden die docker in ihrer Umgebung haben
- Als Anwendung in einem Kubernetes-Cluster direkt oder über Helm

Der Prototyp besteht aus zwei verschiedene Backendservices, welche in Spring Boot implementiert sind:

- [DQN-Backend](https://gitlab.com/multi-component-app/dqn-backend)
- [CMS-Backend](https://gitlab.com/multi-component-app/cms-backend)

Sowie einem Frontend-Service in vueJS/Quasar (abweichend vom tatsächlichen Frontend-Technologie-Stack) zur Veranschaulichung einer mit npm gebauten Single Page Applikation:

- [Frontend](https://gitlab.com/multi-component-app/frontend)

Ein Wrapper Projekt setzt die Gesamtanwendungen aus den Teilanwendungen für einen Kunden zusammen:

- [Kunde 1 Anwendung](https://gitlab.com/multi-component-app/customer-app)

Alle Anwendungen/Services sind jeweils in eigenen Git-Repositorien. Für jedes Repository werden jeweils zwei Artifakte gebaut:

- 1 Docker-Container
- 1 "Natives"-Artifakt
  - JAR/WAR für Spring-Boot
  - JAR/WAR für SPA-Frontend oder alternatives Format

In Rahmen des Prototypen wurde mit GitLab-CI gebaut (jedes Projekt enthält ein .gitlab-ci.yml), da über diesen Weg schnell gebaut werden konnte ohne ein externes Build-Tool wie Jenkins aufzusetzen. Ausserdem wurde die Container-/Package-Registry von Gitlab benutzt.

Die "Wahrheit" des Wrapper-Projektes ist das docker-compose.yml, weil dieses alle Versionen der Einzelteile der Applikation enhält, sowie die Referenzen auf Umgebungsvariablen. Es enthält alle externen Teile der Applikation wie z.B. die Datenbank(en), Elastic Search.

![docker-compose](img/docker-compose.png)

Das Wrapper-Projekt enthält keinen Source-Code. Einzige Logik in diesem Projekt ist eine Art Build-Skript, welches ein einzelenes Artifakt aus den Einzelteile für den jeweiligen Kunden erzeugt. Die Versionen werden dabei aus dem docker-compose gezogen.

Im Rahmen des Prototypen wurde überlegt, ob es Sinn macht für jeden Kunden ein eigenes Wrapper-Projekt zu erstellen. Dies ist aber nicht zwingend erforderlich.

## Anlieferungsszenarien

### Nicht-Docker-Umgebungen

Für das Deployment auf Umgebungen muss die Anwendung gebaut werden. Dies wird über ein Build-Skript bewerkstelligt. Dieses nimmt das docker-compose.yml extrahiert sämtliche für die Anwendung benötigten Artifakte und deren Versionen. Danach lädt er sämtliche internen Artifakte aus dem Artifakt-Repository (z.B. Gitlab oder Nexus). Externe Aritfakte werden von externen Quellen ebenfalls in der entsprechenden Version geladen. Die Anwendung kann dann über ein zentrales Shellskript gestarted, welche alle Anwendungsteile startet.

Die so enstandendene Anwendung kann dann als eine gepackte Datei dem Kunden geliefert werden kann. Dieser muss die Anwendung nur entpacken und über das zentrale Startskript starten. Das gepackte File kann ebenfalls im Artifakt-Repository heruntergeladen werden.

### Docker-Umgebungen

In einer Docker-Umgebung kann die Anwendung über folgendes Kommando im Root-Verzeichnis des Wrapper-Projektes gestarted werden:

```shell
docker-compose up -d
```

Docker zieht nun alle benötigten Container aus den Docker-Registries und started die Gesamtanwendung.

### Kubernetes-Cluster

In einem zusätzlichen Schritt werden aus dem docker-compose-file über das Kommandozeilentool [kompose](https://github.com/kubernetes/kompose) Kubernetes Resourcen oder Helm-Charts erzeugt, welche die Anwendung auf einem Kubernetes-Cluster deploybar machen.

Kubernetes-Resourcen:

```shell
kompose convert
```

Helm Charts:

```shell
kompose convert -j
```

Über die erzeugten Resourcen bzw. Helm-Charts kann die Anwendung dann in Kubernetes deployed werden.

## Offene Punkte/Fragen/Ideen

- Einsatz eines Entwicklungsprozess auf Basis [git-flow](https://www.atlassian.com/de/git/tutorials/comparing-workflows/gitflow-workflow#:~:text=Der%20Git%2Dflow%2DWorkflow%20ist,Release%20des%20Projekts%20konzipiert%20wurde.&text=Git%2Dflow%20ist%20hervorragend%20f%C3%BCr,Release%2DZyklus%20nach%20Zeitplan%20geeignet.) ist empfehlenswert
- Wird ein nginx Reverse Proxy "vor" den Services benötigt?
- Wäre es eventuell eleganter die Frontend-Application über nginx bereitzustellen statt künstlich in einem Java-Webarchiv? Vermutlich auch performanter.
- Wird über Jenkins gebaut wird noch ein Nexus-Repository für die Docker-Container/Maven-Artifakte benötigt
- Verteiltes Logging muss im Auge behalten werden, eventuell werden einzigartige Request-IDs benötigt welche in den Spring-Boot-Anwendung über Zipkin/Sleuth leichtgewichtig erzeugt werden. Eventuell wird Kibana benötigt.
- Verteilte Security liesse sich über OAuth2 realisieren

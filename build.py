
def _load_docker_compose():
    print("load meta data from docker-compose")
    # return bios.read("docker-compose.yml")


def get_containers():

    docker_compose = _load_docker_compose()
    print("extract container meta data")
    print("found internal cms-backend-1.5.8")
    print("found internal dqn-backend-1.3.3")
    print("found internal frontend-1.9.0")
    print("found external elasticsearch-7.9.3")
    print("found external postgres-13.1")


def get_environment_variables():
    print("extract environment variables")


def download_internal_artifacts():
    print("get cms-backend-1.5.8 from internal repository")
    print("get dqn-backend-1.3.3 from internal repository")
    print("get frontend-1.9.0 from internal repository")


def download_external_artifacts():
    print("get elasticsearch-7.9.3 from external resource")
    print("get postgres-13.1 from external resource")


def packing_application():
    print("packing application to zip")


def publishing_application():
    print("publishing packed application to repository")


get_containers()
get_environment_variables()
download_internal_artifacts()
download_external_artifacts()
packing_application()
publishing_application()
